const config = require('config');
const winston = require('winston');
const myCustomLevels = {
    levels: {
      error: 0,
      warn: 1,
      info: 2,
      debug: 3,
      verbose: 4,
      silly: 5
    },
    colors: {
      debug: 'blue',
      info: 'green',
      warn: 'yellow',
      error: 'red',
      verbose: 'turquoise',
      silly: 'pink'
    }
  };

var output = [];

if(config.get('log.console')){
	output.push(new (winston.transports.Console)({level: config.get('log.level.console'), 
	  									colorize: config.get('log.colorize'),
	  									timestamp : config.get('log.timestamp')}));
}

if(config.get('log.file.record')){
	let date = new Date();
	output.push( new (winston.transports.File)({filename: date.toUTCString() + " " +
		config.get('log.file.name'),
		level: config.get('log.level.file'), 
	  	colorize: config.get('log.colorize'),
	  	timestamp : config.get('log.timestamp')}));
}

let logger = new (winston.Logger)({
	transports: output
});

module.exports = logger;